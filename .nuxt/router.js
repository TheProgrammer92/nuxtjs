import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _8f9cfd58 = () => interopDefault(import('..\\pages\\about.vue' /* webpackChunkName: "pages/about" */))
const _745350a7 = () => interopDefault(import('..\\pages\\contact.vue' /* webpackChunkName: "pages/contact" */))
const _4e8c9d10 = () => interopDefault(import('..\\pages\\piano.vue' /* webpackChunkName: "pages/piano" */))
const _c567826a = () => interopDefault(import('..\\pages\\temoignage.vue' /* webpackChunkName: "pages/temoignage" */))
const _10a9d53a = () => interopDefault(import('..\\pages\\tutoriel.vue' /* webpackChunkName: "pages/tutoriel" */))
const _405dd7b4 = () => interopDefault(import('..\\pages\\errors\\404.vue' /* webpackChunkName: "pages/errors/404" */))
const _750bb4f1 = () => interopDefault(import('..\\pages\\errors\\500.vue' /* webpackChunkName: "pages/errors/500" */))
const _5658c808 = () => interopDefault(import('..\\pages\\projets\\galery.vue' /* webpackChunkName: "pages/projets/galery" */))
const _4a237c95 = () => interopDefault(import('..\\pages\\communitie\\_id.vue' /* webpackChunkName: "pages/communitie/_id" */))
const _46095e94 = () => interopDefault(import('..\\pages\\realisation\\_id.vue' /* webpackChunkName: "pages/realisation/_id" */))
const _502d47ce = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _8f9cfd58,
    name: "about"
  }, {
    path: "/contact",
    component: _745350a7,
    name: "contact"
  }, {
    path: "/piano",
    component: _4e8c9d10,
    name: "piano"
  }, {
    path: "/temoignage",
    component: _c567826a,
    name: "temoignage"
  }, {
    path: "/tutoriel",
    component: _10a9d53a,
    name: "tutoriel"
  }, {
    path: "/errors/404",
    component: _405dd7b4,
    name: "errors-404"
  }, {
    path: "/errors/500",
    component: _750bb4f1,
    name: "errors-500"
  }, {
    path: "/projets/galery",
    component: _5658c808,
    name: "projets-galery"
  }, {
    path: "/communitie/:id?",
    component: _4a237c95,
    name: "communitie-id"
  }, {
    path: "/realisation/:id?",
    component: _46095e94,
    name: "realisation-id"
  }, {
    path: "/",
    component: _502d47ce,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
