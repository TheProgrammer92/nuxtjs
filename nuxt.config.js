export default {
    mode: 'spa',
    /*
     ** Headers of the page
     */
    head: {
        title: "TheProgrammer-help",
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            {
                hid: 'description',
                name: 'description',
                content: process.env.npm_package_description || '',
            },
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/logo1.png' },
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Rubik:300,400' },
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons' },
        ],
        script: [{
                src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js',
                type: "text/javascript"
            }, {
                src: 'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js',
                type: "text/javascript"
            },

        ],
    },

    /*
     ** Customize the progress-bar color
     */
    loading: { color: '#fff' },
    /*
     ** Global CSS
     */
    css: [

        '~/assets/sass/app.scss',

    ],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        '~/plugins/app.js'
    ],

    buildModules: [
        '@nuxtjs/tailwindcss',
        '@nuxtjs/vuetify',

    ],
    tailwindcss: {
        exposeConfig: true,
        configPath: '~/plugins/tailwind.config.js'
    },
    /*
     ** Nuxt.js modules
     */
    modules: [],
    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {},
    },
};