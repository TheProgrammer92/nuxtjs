


let containtMixin= {

    data:function () {
        return {
            pathIcone:"https://theprogrammer-pro.local/images/icone/",
            pathImg:"https://theprogrammer-pro.local/images/images/",
            pathLogo:"https://theprogrammer-pro.local/images/logo/",
            pathSvg:'https://theprogrammer-pro.local/images/svg/',
            pathVideo:'https://theprogrammer-pro.local/video/',
            pathPortfolio: '/images/images/portfolio/',
            pathPortfolioRealisation: '/images/images/portfolio/realisation/',

            //users

            pathTemoignage:'https://theprogrammer-pro.local/images/users/temoignage/',
            pathMiniatureUser: 'https://theprogrammer-pro.local/storage/userMiniature/',
            pathUserImg: 'https://theprogrammer-pro.local/storage/user/',



            user: {

                name:'',
                avatar:''
            }
        }
    },
    methods: {

        redirectError:function (error) {
            let app = this;


            console.log(error.response);

            if (error.response.data.status=="500") {

                app.$router.push("/500");

            }

            else if(error.response.data.status=="404") {

                app.$router.push("/404");
            }

            else if(error.response.data.status=="401") {

                app.$router.push("/401");
            }

        },

        getInfoUser:function() {

            //now retrieval of user information
            let data = axios.get('auth/user')
                .then(function(response){

                    return response

                });



            return data;
        },

        getInfo :function() {

            let promise = this.getInfoUser();
            let app= this;
            promise.then(function (response) {



                app.user.avatar = app.pathMiniatureUser + response.data.data.avatar;






            })
                .catch(e => {
                    console.log("errror mixin global")
                    console.log(e.response)
                })

        }
    },
    created(){

        //initialising: materialize
        $(function () {

            M.AutoInit();







        })
    },




}


export default  containtMixin
