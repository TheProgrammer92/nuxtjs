export const state = () => ({
  tab_communitie: [
      {

        title: "DISCORD",
       
        message:
         "Le lycée général se compose :De secondes générales et technologiques" +
         "e déterminatio Du baccalauréat général Du baccalauréat STI2D" +
         "Pour découvrir les différentes options de secondes et" +
         "des baccalauréat veuillez consulter les pages ci-contre" +
         "e sdsdfddéterminatio Du baccalauréat général Du baccalauréat STI2D" +
         "Pour découvrir les différentes options de secondes et" +
         "des baccalauréat veuillez consulter les pages ci-contre",
       img: "discord.PNG",
       nbr:"PLUS DE 700",
       link:"https://discord.gg/8SyZ7GA",
       id:1
      },  {

        title: "WHATSSAP GROUPE",
       
        message:
         "Le lycée général se compose :De secondes générales et technologiques" +
         "e déterminatio Du baccalauréat général Du baccalauréat STI2D" +
         "Pour découvrir les différentes options de secondes et" +
         "des baccalauréat veuillez consulter les pages ci-contre" +
         "e sdsdfddéterminatio Du baccalauréat général Du baccalauréat STI2D" +
         "Pour découvrir les différentes options de secondes et" +
         "des baccalauréat veuillez consulter les pages ci-contre",
       img: "whatssap.PNG",
       link:"https://chat.whatsapp.com/IfJKcyXRN0h2mb9CJCvbip",
       id:2,

    
       nbr:"PLUS DE 250"
      },
  ],
  tab_realisation: [
    {
      id: 1,
      title: "Gestion des Etudiants dans une université",
      message:
        "Le lycée général se compose :De secondes générales et technologiques" +
        "e déterminatio Du baccalauréat général Du baccalauréat STI2D" +
        "Pour découvrir les différentes options de secondes et" +
        "des baccalauréat veuillez consulter les pages ci-contre" +
        "e sdsdfddéterminatio Du baccalauréat général Du baccalauréat STI2D" +
        "Pour découvrir les différentes options de secondes et" +
        "des baccalauréat veuillez consulter les pages ci-contre",
      img: "C2.jpg",
      langage:"C#",
      other_img:['C1.jpg','C3.jpg']
    },
    {
      id: 2,
      title: "Application de Gestion et envoie des courrier",
      message:
        "Le lycée général se compose :De secondes générales et technologiques" +
        "e déterminatio Du baccalauréat général Du baccalauréat STI2D" +
        "Pour découvrir les différentes options de secondes et" +
        "des baccalauréat veuillez consulter les pages ci-contre" +
        "e déterminatio Du baccalauréat général Du baccalauréat STI2D" +
        "Pour découvrir les différentes options de secondes et" +
        "des baccalauréat veuillez consulter les pages ci-contre",
      img: "courrier2.png",
      langage:"Laravel + Vue.js",
      other_img:["courrier3.png",'courrier4.png','courrier6.png']
    },
    {
      id: 3,
      title: "Site web , presentation des Montres",
      message:
        "Le lycée général se compose :De secondes générales et technologiques" +
        "e déterminatio Du baccalauréat général Du baccalauréat STI2D" +
        "Pour découvrir les différentes options de secondes et" +
        "des baccalauréat veuillez consulter les pages ci-contre" +
        "e déterminatio Du baccalauréat général Du baccalauréat STI2D" +
        "Pour découvrir les différentes options de secondes et" +
        "des baccalauréat veuillez consulter les pages ci-contre",
      img: "horlogiz2.jpg",
      langage:"Larave + Vue.js",
      other_img:["horlogiz1.jpg",'horlogiz2.jpg']
    },
    
    {
      id: 4,
      title: "Un projet de gestion des étudiants + notifications par sms",
      message:
        "Le lycée général se compose :De secondes générales et technologiques" +
        "e déterminatio Du baccalauréat général Du baccalauréat STI2D" +
        "Pour découvrir les différentes options de secondes et" +
        "des baccalauréat veuillez consulter les pages ci-contre" +
        "e déterminatio Du baccalauréat général Du baccalauréat STI2D" +
        "Pour découvrir les différentes options de secondes et" +
        "des baccalauréat veuillez consulter les pages ci-contre",
      img: "horlogiz2.jpg",
      langage:"Java ,Javafx",
      other_img:["student_manage.jpg",'student_manage2.jpg',
      'student_manage5.jpg','student_manage4.jpg']
    },
    
    {
      id: 5,
      title: "ZILI , un site de vente pour un particulier",
      message:
        "Le lycée général se compose :De secondes générales et technologiques" +
        "e déterminatio Du baccalauréat général Du baccalauréat STI2D" +
        "Pour découvrir les différentes options de secondes et" +
        "des baccalauréat veuillez consulter les pages ci-contre" +
        "e déterminatio Du baccalauréat général Du baccalauréat STI2D" +
        "Pour découvrir les différentes options de secondes et" +
        "des baccalauréat veuillez consulter les pages ci-contre",
      img: "zili5.png",
      langage:"Java ,Javafx",
      other_img:["zili1.png",'zili2.png',"zili3.png",'zili4.png']
    },
    
    {
      id: 6,
      title: "Site officiel, d'une communauté chrétienne international",
      message:
        "Le lycée général se compose :De secondes générales et technologiques" +
        "e déterminatio Du baccalauréat général Du baccalauréat STI2D" +
        "Pour découvrir les différentes options de secondes et" +
        "des baccalauréat veuillez consulter les pages ci-contre" +
        "e déterminatio Du baccalauréat général Du baccalauréat STI2D" +
        "Pour découvrir les différentes options de secondes et" +
        "des baccalauréat veuillez consulter les pages ci-contre",
      img: "Capture2.PNG",
      langage:"Java ,Javafx",
      other_img:["capture1.PNG"]
    },
  ],
});

export const mutations = {
  //les cours

  async GET_SERVICE_SLUG(state, slug) {
    let service = (await this.$axios.$get("api/service/slug?slug=" + slug))
      .data;

    state.tab_service = [];
    state.tab_service = service;
  },
  //les cours

  async GET_ALL_DONATE(state) {
    let donate = (await this.$axios.$get("api/donate/all/")).data;

    state.tab_donate = [];
    state.tab_donate = donate;
  },

  async GET_ALL_OPPORTUNITIE(state) {
    let opportunities = (await this.$axios.$get("api/opportunitie/all/")).data;

    state.tab_opportunities = [];
    state.tab_opportunities = opportunities;
  },

  async GET_ALL_TRAINING(state) {
    let training = (await this.$axios.$get("api/training/all/")).data;

    state.tab_training = [];
    state.tab_training = training;
  },

  async GET_ALL_ABOUT(state) {
    let about = (await this.$axios.$get("api/about/all/")).data;

    state.tab_about = [];
    state.tab_about = about;
  },

  async GET_ALL_EQUIPMENT(state) {
    let equipment = (await this.$axios.$get("api/equipment/all/")).data;
    state.tab_equipment = [];
    state.tab_equipment = equipment.data;
  },
};

export const actions = {
  getAllOpportunities({ commit }) {
    commit("GET_ALL_OPPORTUNITIE");
  },
  getAllTraining({ commit }) {
    commit("GET_ALL_TRAINING");
  },
  getAllAbout({ commit }) {
    commit("GET_ALL_ABOUT");
  },
  getAllEquipment({ commit }) {
    commit("GET_ALL_EQUIPMENT");
  },

  getServiceSlug({ commit }, slug) {
    commit("GET_SERVICE_SLUG", slug);
  },
  getAllDonate({ commit }) {
    commit("GET_ALL_DONATE");
  },
};

export const getters = {
  tab_training: (state) => state.tab_training,
  tab_about: (state) => state.tab_about,
  tab_training: (state) => state.tab_training,
  tab_opportunities: (state) => state.tab_opportunities,
  tab_service: (state) => state.tab_service,
  tab_donate: (state) => state.tab_donate,
  tab_personnal: (state) => state.tab_personnal,
  tab_realisation: (state) => state.tab_realisation,
  tab_communitie: (state) => state.tab_communitie,
};
